import 'package:dart_application_midterm/dart_application_midterm.dart'
    as dart_application_midterm;

import 'dart:io';

List tokenizing(String ex) {
  var tokens0 = ex.split('');
  // print(tokens0);
  var tokensl = new List<String>.filled(0, "m", growable: true);
  for (var token in tokens0) {
    // for (var i = 0; i < tokens0.length; i++) {
    if (token != " ") {
      // if (tokens0[i] != " ") {
      tokensl.add(token); // tokensl.add(tokens0[i]);
    }
  }
  // print(tokensl);

  /* don't use this because set is keep only unique string then not only space lost 
  but also dupplicating string are lost too 

  var tokenss = <String>{};
  tokenss.addAll(tokens0);
  tokenss.remove(" ");
  print(tokenss);*/

  //test : "5           +           3 + 1 -2-34%2-6^4%2"
  //should be : 5, +, 3, +, 1, -, 2, -, 34, %, 2, -, 6, ^, 4, %, 2

  /*  focus on operator "+ - * / % ^ ( )" and "number 0-9" then
  test : "5    +           3 + 1 -(2-3)%2-6^4%2"
  should be : 5, +, 3, +, 1, -, (, 2, -, 3, ), %, 2, -, 6, ^, 4, %, 2
  */

  return tokensl;
}

bool isInteger(String str) {
  for (var i = 0; i <= 9; i++) {
    try {
      if (int.parse(str) == i) {
        return true;
      }
    } on Exception catch (e) {}
  }
  return false;
}

bool isOperator(String str) {
  for (String t in {"+", "-", "*", "/", "%", "^"}) {
    if (str.compareTo(t) == 0) {
      return true;
    }
  }
  return false;
}

int precedences(String str) {
  switch (str) {
    case "+":
    case "-":
      return 1;
    case "*":
    case "/":
    case "%":
      return 2;
    case "^":
      return 3;
    default:
      return 0;
  }
}

List infixToPostfix(List infix) {
  var oprList = new List<String>.filled(0, "m", growable: true);
  var postfix = new List<String>.filled(0, "m", growable: true);

  for (var token in infix) {
    if (isInteger(token)) {
      postfix.add(token);
    }
    if (isOperator(token)) {
      while (oprList.isNotEmpty &&
          (oprList.last.compareTo("(") != 0) &&
          (precedences(token) <= precedences(oprList.last))) {
        postfix.add(oprList.removeLast());
      }
      oprList.add(token);
    }
    if (token.compareTo("(") == 0) {
      oprList.add(token);
    }
    if (token.compareTo(")") == 0) {
      while (oprList.last.compareTo("(") != 0) {
        postfix.add(oprList.removeLast());
      }
      oprList.removeLast();
    }
  }
  while (oprList.isNotEmpty) {
    postfix.add(oprList.removeLast());
  }
/*
[5, +, 3, +, 1, -, (, 2, -, 3, ), %, 2, -, 6, ^, 4, %, 2]

symbol ========== oprList ========== postfix
5                   n                   5
+                   +                   5
3                   +                   53
+                   +                   53+
1                   +                   53+1
-                   -                   53+1+
(                   -(                  53+1+
2                   -(                  53+1+2
-                   -(-                 53+1+2
3                   -(-                 53+1+23
)                   -                   53+1+23-
%                   -%                  53+1+23-
2                   -%                  53+1+23-2
-                   -                   53+1+23-2%-
6                   -                   53+1+23-2%-6
^                   -^                  53+1+23-2%-6
4                   -^                  53+1+23-2%-64
%                   -%                  53+1+23-2%-64^
2                   -%                  53+1+23-2%-64^2
                                        53+1+23-2%-64^2%-
 */

  return postfix;
}

void main() {
  String ex = stdin.readLineSync()!;
  var tokens = tokenizing(ex);
  print("Your list of tokens : $tokens");
  var postfix = infixToPostfix(tokens);
  print("Your list of postfix : $postfix");
  print("Your postfix :");
  print(postfix.join(",").replaceAll(",", ""));
  // for (String pf in postfix) {
  //   stdout.write(pf);
  // }
}
